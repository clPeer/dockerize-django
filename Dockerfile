# Pull base image
FROM python:3.7-slim

# Set environment varibles
ENV PYTHONUNBUFFERED 1

RUN mkdir /code

# Set work directory
WORKDIR /code

COPY requirements.txt /code/

# Install dependencies
RUN pip install -r requirements.txt

# Copy project
COPY . /code/

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]